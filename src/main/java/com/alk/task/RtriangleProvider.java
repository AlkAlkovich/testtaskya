package com.alk.task;

/**
 * Created by Alk on 26.06.2015.
 */
public final class RtriangleProvider implements Rtriangle {

    private int apexX1 = 2;
    private int apexY1 = 7;
    private int apexX2 = 9;
    private int apexY2 = 3;
    private int apexX3 = 5;
    private int apexY3 = 9;

    public int getApexX1() {
        return apexX1;
    }

    public int getApexY1() {
        return apexY1;
    }

    public int getApexX2() {
        return apexX2;
    }

    public int getApexY2() {
        return apexY2;
    }

    public int getApexX3() {
        return apexX3;
    }

    public int getApexY3() {
        return apexY3;
    }

    public static Rtriangle getRtriangle() {
        return new RtriangleProvider();
    }

}