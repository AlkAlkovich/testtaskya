package com.alk.task;

/**
 * Created by Alk on 26.06.2015.
 */
public class TestRtriangle {
    public static void main(String[] args) {
        testGetRtriangle();
    }

    private static void testGetRtriangle() {
        Rtriangle r = RtriangleProvider.getRtriangle();
        int x1 = r.getApexX1();
        int y1 = r.getApexY1();
        int x2 = r.getApexX2();
        int y2 = r.getApexY2();
        int x3 = r.getApexX3();
        int y3 = r.getApexY3();

        double sideAB =  Math.sqrt(Math.pow(x2 - x1, 2) + Math.pow(y2 - y1, 2));
        double sideBC =  Math.sqrt(Math.pow(x3 - x2, 2) + Math.pow(y3 - y2, 2));
        double sideAC =  Math.sqrt(Math.pow(x1 - x3, 2) + Math.pow(y1 - y3, 2));

        boolean A=Math.pow(sideAB,2)==(Math.pow(sideBC,2)+Math.pow(sideAC,2));
        boolean B=Math.pow(sideBC,2)==(Math.pow(sideAB,2)+Math.pow(sideAC,2));
        boolean C=Math.pow(sideAC,2)==(Math.pow(sideBC,2)+Math.pow(sideAB,2));

        boolean result = A || B || C;
        Assert.assertEquals(true,result);

    }
}
