package com.alk.task;

/**
 * Created by Alk on 26.06.2015.
 */
public interface Rtriangle {

    int getApexX1();

    int getApexY1();

    int getApexX2();

    int getApexY2();

    int getApexX3();

    int getApexY3();

}
